import time
import random


def main():
    print("Hello algorithm")
    my_array = []
    my_array = sorted(create_string_list()) # we can switch to integer list 'create_integer_list()'
    print(f'String List Ordered: {my_array}')
    element = 'Boca Juniors'
    #my_array = collatz_algorithm(element)
    #use_collatz(element)
    linear_search(my_array, element)
    binary_search(my_array, element)
    exponential_search(my_array, element)


def create_integer_list():
    my_list = []
    for i in range(0, 2000):
        n = random.randint(1, 3000)
        my_list.append(n)
    return my_list


def create_string_list():
    my_list = ['Damian', 'Python', 'Go', 'Spinosa', 'Carlos', 'Javascript',
               'Java', 'Kubernetes', 'Docker', 'Suyay', 'Code', 'Vue', 'Vue.js', 'Boca Juniors',
               'Apple', 'Aws', 'Linux']
    return my_list


def linear_search(my_array, element):
    founded = -1
    start = time.time()
    print('Linear Search .....')
    for i in range (len(my_array)):
        if my_array[i] == element:
            founded = element

    end = time.time()
    print(f"-------------{linear_search.__name__}-----------------")
    print(f"time: {end-start}")
    print(f"Founded: {founded}")


def binary_search(my_array, element, response=False):
    first = 0
    last = len(my_array) - 1
    index = -1
    start = time.time()
    if not response:
        print('Binary Search .....')
    while (first <= last) and (index == -1):
        mid = (first+last) // 2  # divide between first and last position
        if my_array[mid] == element:
            index = mid
        else:
            if element < my_array[mid]:
                last = mid - 1  # set last position as middle less 1
            else:
                first = mid + 1   # set first position as middle add 1

    end = time.time()
    position = index
    if response:
        return position
    time_result = end - start
    print_result = f"{time_result:.9f}"
    print(f"-------------{binary_search.__name__}-----------------")
    print(f"time: {print_result}")
    print(f"Founded: {position}")


def exponential_search(my_array, element):
    print(f"-------------{exponential_search.__name__}-----------------")
    start = time.time()
    if my_array[0] == element:
        print(f'found element: {my_array[0]}')
    index = 1
    while index < len(my_array) and my_array[index] <= element:
        index = index * 2
    founded = binary_search(my_array[:min(index, len(my_array))], element=element, response=True)
    end = time.time()
    print_time = f"{(end-start):.9f}"
    print(f"time: {print_time}")
    print(f"Founded: {founded}")


def collatz_algorithm(n: int) -> list:
    my_list = []
    while n != 1:
        my_list.append(n)
        n = n // 2 if n % 2 == 0 else n * 3 + 1
    else:
        my_list.append(1)
        print(f"Collatz Algorithm: {my_list}")
        return my_list


def use_collatz(element: int):
    for i in collatz_algorithm(element):
        print(i, end=" ")
        if element // i == 2:
            break


if __name__ == '__main__':
    main()
