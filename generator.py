import random, time
from sys import getsizeof


element = -1234


#@profile(precision=4)
# use profile
# pip install -U memory_profiler
# run -> python3 -m memory_profiler generator.py
def main():
    list_to_use = create_list()
    with_out_generator(list_to_use)
    test_generator()


def create_list():
    my_array = []
    for _ in range(0, 200000):
        n = random.randint(1, 400000)
        my_array.append(n)
    return my_array


def create_generator():
    for _ in range(0, 20000):
        n = random.randint(1, 40000)
        yield n


#@profile(precision=4)
def with_out_generator(list_to_use: list):
    print(f' ------- Test with out use generators **** using list -------------- ')
    start = time.time()
    memory = getsizeof(list_to_use)
    print(f'Use of memory: {memory}')
    for i in list_to_use:
        if i == element:
            print(f'element founded. {i}')
    print(f'element not founded.')
    end = time.time()
    print(f"time: {end - start}")


#@profile()
def test_generator():
    print(f' ------- Test using generators  -------------- ')
    start = time.time()
    memory = getsizeof(create_generator())
    print(f'Use of memory: {memory}')
    for i in create_generator():
        if i == element:
            print(f'element founded. {i}')
    print(f'element not founded.')
    end = time.time()
    print(f"time: {end - start}")


if __name__ == '__main__':
    main()
